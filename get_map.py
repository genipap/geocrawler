#!/usr/bin/python

import os
import urllib
import argparse
import time
from PIL import Image
from random import randint
import requests

def get_image(url, image_name):

    urllib.urlretrieve(url, image_name)

    ### First try
    try:
        im = Image.open(image_name)
    except IOError, e:

        print 'IOError, reloading image ' + image_name
        print 'Waiting ...'
        time.sleep(10)

        ### Second try
        urllib.urlretrieve(url, image_name)

        try:
            im = Image.open(image_name)
        except IOError, e2:

            print 'Reloading failed ... now change another way to download image ...'
            print 'Waiting ...'
            time.sleep(10)

            ### Third try, change another way to download image
            r = requests.get(url)
            with open(image_name, 'wb') as f:
                for chunk in r.iter_content():
                    f.write(chunk)
            try:
                im = Image.open(image_name)
            except IOError, e3:

                print 'Give up ... create a empty image instead'
                print 'Failed to download image ' + image_name
                empty_img = Image.new('RGB', (256, 256))
                empty_img.save(image_name)
                im = Image.open(image_name)
    return im


def get_map(args):

    ### get args
    row_start = int(args.get('row', 11320))
    col_start = int(args.get('column', 16620))
    row_span = col_span = int(args.get('span', 5))
    matrix = int(args.get('matrix', 15)) or 15
    output = args.get('output', 'my_map.jpg') or 'my_map.jpg'
    print 'output file: ' + output

    url_prefix = 'http://wxs.ign.fr/tyujsdxmzox31ituc2uw0qwl/geoportail/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&&LAYER=GEOGRAPHICALGRIDSYSTEMS.MAPS&STYLE=normal&FORMAT=image/jpeg&TILEMATRIXSET=PM&'
    url_appendix = '&extParamId=aHR0cDovL3d3dy5nZW9wb3J0YWlsLmdvdXYuZnIvYWNjdWVpbA=='

    ### Create an empty map
    my_map = Image.new('RGB', (256 * col_span, 256 * row_span))

    for row in xrange(row_span):

        print 'Saving row: ' + str(row_start + row) + '...'

        for col in xrange(col_span):

            image_name = str(row_start + row) + '-' + str(col_start + col) + '.jpg'

            ### First, download image from url
            url = url_prefix + 'TILEMATRIX=' + matrix +'&TILEROW=' + str(row_start + row) + '&TILECOL=' + str(col_start + col) + url_appendix
            im = get_image(url, image_name)

            ### Then, paste image to map
            my_map.paste(im, (col * 256, row * 256))

            ### Last, remove tile image
            os.remove(image_name)
            time.sleep(0.3)

        print 'Calm down, don\'t be too violent ...'
        time.sleep(randint(3, 5))

    print 'Combining image ...'
    my_map.save(output)
    print 'Image ' + output + ' saved successfully !'

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Script for getting map from geoportail.fr')
    parser.add_argument('--row', action='store', help='Row of start point', metavar='<ROW_NUM>', required=True)
    parser.add_argument('--column', action='store', help='Column of start point', metavar='<COL_NUM>', required=True)
    parser.add_argument('--span', action='store', help='Size of map (number of tile map)', metavar='<SPAN>', required=True)
    parser.add_argument('--matrix', action='store', help='The tile matrix number', metavar='<MATRIX>')
    parser.add_argument('--output', action='store', help='Output file name', metavar='<OUTPUT>')
    parser.add_argument('--version', action='version', version='%(prog)s 1.0.0')

    args = vars(parser.parse_args())
    get_map(args)
